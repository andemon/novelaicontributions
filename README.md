# NovelAI Community Repository
An early community contribution for the NovelAI project

## About this repository
It is an attempt to summarize and centralize community contributions for NovelAI  
It is maintained by Noli, Lion and Javaman, which are not official part of the NovelAI team  

## Who is this repo meant for
This repo is meant for anyone that wants to contribute to the community effort in the NovelAI project

## I want to contribute
Good! You can clone this repo and make a merge request, We will be happy to get some activity here!

## On a side note
Note to official NovelAI team members that may read this, you are welcome on this repo. Collaboration and communication between the community collaborators and the official staff is important, the sooner the better
